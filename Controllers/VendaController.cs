using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("controller")]
    public class VendaController : ControllerBase
    {
        private readonly BDVendaContext _context;

        public VendaController(BDVendaContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<List<Venda>>> Get(int vendedorId)
        {
            var vendas = await _context.Vendas
            .Where(x => x.VendedorId == vendedorId)
            .Include(i => i.ItensVenda)
            .ToListAsync();

            return vendas;
        }
        
        [HttpGet("BuscaVendaId")]
        public async Task<ActionResult<List<Venda>>> GetVenda(int vendaId)
        {
            var vendas = await _context.Vendas
            .Where(x => x.Id == vendaId)
            .Include(i => i.ItensVenda)
            .ToListAsync();

            return vendas;
        }

        [HttpPost("Venda")]
        public async Task<ActionResult<List<Venda>>> Create(CreateVendaDTO request)
        {
            var vendedor = await _context.Vendedores.FindAsync(request.VendedorId);
            if(vendedor == null)
                return NotFound();

            request.DataVenda = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            request.StatusVenda = EnumStatusVenda.Aguardano_Pagamento;
            
            var newVenda = new Venda
            {
                DataVenda = request.DataVenda,
                StatusVenda = request.StatusVenda,
                Vendedor = vendedor
            };

            _context.Vendas.Add(newVenda);
            await _context.SaveChangesAsync();

            return await Get(newVenda.VendedorId);
        }

         [HttpPost("ItensVenda")]
        public async Task<ActionResult<Venda>> AddItemVenda (AddItemVendaDTO request)
        {
            var venda = await _context.Vendas.FindAsync(request.VendaId);
            if(venda == null)
                return NotFound();

            if((int)venda.StatusVenda != 1)
                return NotFound($"Venda com status de '{venda.StatusVenda}'!");

            var newItemVenda = new ItensVenda
            {
                Produto = request.Produto,
                Preco = request.Preco,
                Venda = venda
            };

            _context.ItensVendas.Add(newItemVenda);
            await _context.SaveChangesAsync();

            return venda;
        }

        [HttpPut("AtualizarStatusVenda")]
        public IActionResult AtualizarStatusVenda(int idVenda, EnumStatusVenda status)
        {
            var vendaBanco = _context.Vendas.Find(idVenda);

            int statusBanco = ((int)vendaBanco.StatusVenda);
            int statusRequirido = ((int)status);
            string mensagem = "";
            string mensagem1 =  $"Alteração de status da venda nº {idVenda} para '{status}' não válida!\nSituação da Venda atual '{vendaBanco.StatusVenda}'!";

            if (vendaBanco == null)
            {
                return NotFound();
            }
            else if(statusBanco == 4 || statusBanco == 5)
            {                
                mensagem = $"Não é possível alterar o status da venda nº {idVenda}, pois, está com status de '{vendaBanco.StatusVenda}'!";
                return BadRequest(mensagem);
            }
            else if(statusRequirido == 0)
            {
                mensagem = $"Alteração de status para '--' não é válida!\n"
                + $"Situação atual da Venda é '{vendaBanco.StatusVenda}!'";
                return BadRequest(mensagem);
            }
            else if(statusRequirido == 1 && (statusBanco != 0))
            {
                return BadRequest(mensagem1);
            }
            else if(statusRequirido == 2 && (statusBanco != 1))
            {
                return BadRequest(mensagem1);
            }else if(statusRequirido == 3 && (statusBanco != 2))
            {
                return BadRequest(mensagem1);
            }else if(statusRequirido == 4 && (statusBanco != 3))
            {
               return BadRequest(mensagem1);
            }
            vendaBanco.StatusVenda = status;

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();
            return Ok(vendaBanco);
        }
    }
}