using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("controller")]
    public class VendedorController : ControllerBase
    {
        private readonly BDVendaContext _context;

        public VendedorController(BDVendaContext context)
        {
            _context = context;
        }
        
        [HttpPost("AdicionarVendedor")]
        public IActionResult CadastroVendedor(AddVendedorDTO request)
        {
            var newVendedor = new Vendedor
            {
                Cpf = request.Cpf,
                Nome = request.Nome,
                Email = request.Email,
                Telefone = request.Telefone
            };

            _context.Add(newVendedor);
            _context.SaveChanges();

            return Ok(newVendedor);
        }

        [HttpGet("TodosVendedores")]
        public async Task<ActionResult<List<Vendedor>>> Get()
        {
            var vendedores = await _context.Vendedores
            .ToListAsync();

            return vendedores;
        }

        [HttpGet("Vendedor")]
        public async Task<ActionResult<List<Vendedor>>> Get(int vendedorId)
        {
            var vendedores = await _context.Vendedores
            .Where (v => v.Id == vendedorId)
            .ToListAsync();

            return vendedores;
        }

    }
}