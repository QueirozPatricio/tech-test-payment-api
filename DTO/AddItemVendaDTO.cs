using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.DTO
{
    public class AddItemVendaDTO
    {               
        public string Produto { get; set; } = String.Empty;
        public decimal Preco { get; set; } = 0.00M;
        public int VendaId { get; set; } = 1; 
    }
}