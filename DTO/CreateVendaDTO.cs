using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.DTO
{
        public class CreateVendaDTO
    {
        public DateTime DataVenda { get; set; } = DateTime.Now;
        public EnumStatusVenda StatusVenda { get; set; } = EnumStatusVenda.Aguardano_Pagamento;
        public int VendedorId { get; set; } = 1;
    }
}