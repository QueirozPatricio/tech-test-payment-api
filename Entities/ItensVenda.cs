using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace tech_test_payment_api.Entities
{
    public class ItensVenda
    {
        public int Id { get; set; }        
        public string Produto { get; set; } = String.Empty;
        public decimal Preco { get; set; } = 0.00M;
        [JsonIgnore]
        public Venda Venda { get; set; }        
        public int VendaId { get; set; } 
    }
}