using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models;
using System.Text.Json.Serialization;

namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public DateTime DataVenda { get; set; }        
        public EnumStatusVenda  StatusVenda { get; set; }
        [JsonIgnore]
        public Vendedor Vendedor { get; set; }        
        public int VendedorId { get; set; }
        public List<ItensVenda> ItensVenda { get; set; }

    }
}