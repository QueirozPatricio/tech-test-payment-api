using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace tech_test_payment_api.Entities
{
    public class Vendedor
    {
        public int Id { get; set; }
        public string Cpf { get; set; } = String.Empty;
        public string Nome { get; set; } = String.Empty;
        public string Email { get; set; } = String.Empty;
        public string Telefone { get; set; } = String.Empty;
        [JsonIgnore]
        public List<Venda> Vendas { get; set; }
    }
}