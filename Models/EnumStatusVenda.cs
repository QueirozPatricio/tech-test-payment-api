namespace tech_test_payment_api.Models
{
    public enum EnumStatusVenda
    {
        Aguardano_Pagamento = 1,
        Pagamento_Aprovado = 2,
        Enviado_para_Transportadora = 3,
        Entregue = 4,
        Cancelada = 5
    }
}